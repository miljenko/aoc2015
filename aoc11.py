input = 'cqjxjnds'

def increasing(s):
    for i in range(0, len(s) - 2):
        if ord(s[i]) == ord(s[i + 1]) - 1 == ord(s[i + 2]) - 2:
            return True
    return False

def twopairs(s):
    c = 0
    last = -1
    for i in range(0, len(s) - 1):
        if s[i] == s[i+1] and last != i:
            c += 1
            last = i + 1
        if c == 2:
            return True
    return False

def replacelastz(input):
    input = input[:-1] + 'a'
    for i in range(len(input) - 2, 0, -1):
        if input[i] == 'z':
            input = input[:i] + 'a' + input[i+1:]
        else:
            input = input[:i] + chr(ord(input[i]) + 1) + input[i+1:]
            break

    return input

def nextpass(input):
    while True:
        last = input[-1]
        if last == 'z':
            input = replacelastz(input)
            if increasing(input) and twopairs(input):
                return input

        for replacement in range(ord(last) + 1, ord('z') + 1):
            new = chr(replacement)
            if new in 'iol':
                continue
            input = input[:-1] + new
            # print input

            if increasing(input) and twopairs(input):
                return input

            if new == 'z':
                input = replacelastz(input)
                if increasing(input) and twopairs(input):
                    return input

new_pass = nextpass(input)
print(new_pass)
print(nextpass(new_pass))
