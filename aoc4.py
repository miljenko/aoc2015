import hashlib

key = 'yzbqklnj'

i = 1
while True:
    m = hashlib.md5((key + str(i)).encode())
    hash = m.hexdigest()
    if hash.startswith('00000'):
        print(i)
        print(hash)
    if hash.startswith('000000'):
        print(i)
        print(hash)
        break
    if i % 10000 == 0:
        print(i)
    i += 1
