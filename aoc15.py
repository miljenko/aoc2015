import re
from functools import reduce
from itertools import combinations_with_replacement, permutations
from operator import mul

ingrs = []
ingrvals = []
spoons = 100

with open('input15.txt') as f:
    for line in f:
        ingredient = line.split(':')[0]
        ingrs.append(ingredient)
        nums = [int(n) for n in re.findall('-?\d+', line)]
        ingrvals.append(nums)

print(ingrs)
print(ingrvals)

max_score = 0

for combo in combinations_with_replacement(range(spoons + 1), len(ingrs)):
    if sum(combo) != spoons:
        continue

    for perm in permutations(combo):
        weighted = [[v * perm[i] for v in ingrvals[i]]
                    for i in range(len(ingrs))]
        if sum(w[4] for w in weighted) != 500:  # calories
            continue

        totals = [max(sum(w[i] for w in weighted), 0)
                  for i in range(4)]  # up to 4 - no calories

        score = reduce(mul, totals)
        if score > max_score:
            max_score = score

print(max_score)
