total = 0

def has3(s):
    n = 0
    for c in s:
        if c in 'aeiou':
            n += 1
            if n == 3:
                return True
    return False

def inarow(s):
    for i in range(len(s) - 1):
        if s[i] == s[i+1]:
            return True
    return False

def nothave(s):
    bad = ['ab', 'cd', 'pq', 'xy']
    if any(b in s for b in bad):
        return False
    return True

def pairtwice(s):
    for i in range(len(s) - 1):
        if s.count(s[i:i+2]) > 1:
            return True
    return False

def onebetween(s):
    for i in range(len(s) - 2):
        if s[i] == s[i+2]:
            return True
    return False

with open('input5.txt') as f:
    for s in f:
        # if has3(s) and inarow(s) and nothave(s):
        if pairtwice(s) and onebetween(s):
            total += 1

print(total)
