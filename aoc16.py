aunts = {}

gift = {
    'children': 3,
    'cats': 7,
    'samoyeds': 2,
    'pomeranians': 3,
    'akitas': 0,
    'vizslas': 0,
    'goldfish': 5,
    'trees': 3,
    'cars': 2,
    'perfumes': 1
}

with open('input16.txt') as f:
    for line in f:
        sue, compstr = line.split(':', 1)
        comps = {}
        match = True
        for comp in compstr.split(','):
            l = comp.split(':')
            comp, val = l[0].strip(), int(l[1].strip())
            comps[comp] = val

            if comp in ('cats', 'trees'):
                if val <= gift[comp]:
                    match = False
            elif comp in ('pomeranians', 'goldfish'):
                if val >= gift[comp]:
                    match = False
            elif val != gift[comp]:
                match = False

        if match:
            aunts[sue] = comps

print(aunts)
