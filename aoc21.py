from itertools import permutations

boss = (104, 8, 1)  # hit, damage, armor
player = (100, 0, 0)

weapons = [(8, 4), (10, 5), (25, 6), (40, 7), (74, 8)]
armors = [(0, 0), (13, 1), (31, 2), (53, 3), (75, 4), (102, 5)]
rings = [(0, 0, 0), (0, 0, 0), (25, 1, 0), (50, 2, 0),
         (100, 3, 0), (20, 0, 1), (40, 0, 2), (80, 0, 3)]


def attack(player, boss):
    while True:
        damage = max(player[1] - boss[2], 1)
        boss[0] -= damage
        if boss[0] <= 0:
            # print 'Player won'
            return True
        damage = max(boss[1] - player[2], 1)
        player[0] -= damage
        if player[0] <= 0:
            # print 'Boss won'
            return False

minpw = 1000
maxpl = 0

for weapon in weapons:
    for armor in armors:
        for r1, r2 in permutations(rings, 2):
            # print weapon, armor, r1, r2
            p = list(player)
            b = list(boss)
            p[1] += weapon[1] + r1[1] + r2[1]
            p[2] += armor[1] + r1[2] + r2[2]
            won = attack(p, b)
            cost = weapon[0] + armor[0] + r1[0] + r2[0]
            if won and (cost < minpw):
                minpw = cost
            if (not won) and (cost > maxpl):
                maxpl = cost

print('Minimum cost to win =', minpw)
print('Maximum cost to lose =', maxpl)
