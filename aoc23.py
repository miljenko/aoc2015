program = []
registers = {'a': 1, 'b': 0}

with open('input23.txt') as f:
    for line in f:
        instr, params = line[:3], [p if p in registers else int(p)
                                   for p in line[3:].strip().split(', ')]
        program.append((instr, params))

lineno = 0
while lineno < len(program):
    instr, params = program[lineno]

    if instr == 'hlf':
        registers[params[0]] //= 2
    elif instr == 'tpl':
        registers[params[0]] *= 3
    elif instr == 'inc':
        registers[params[0]] += 1
    elif instr == 'jmp':
        lineno += params[0]
        continue
    elif instr == 'jie':
        if registers[params[0]] % 2 == 0:
            lineno += params[1]
            continue
    elif instr == 'jio':
        if registers[params[0]] == 1:
            lineno += params[1]
            continue
    else:
        break

    lineno += 1

print(registers)
print(registers['b'])
