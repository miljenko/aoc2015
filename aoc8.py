import ast

diff = 0
diff2 = 0
i = 0
with open('input8.txt') as f:
    for line in f:
        line = line.strip()
        diff += len(line) - len(ast.literal_eval(line))

        add = 0
        for i in range(len(line)):
            # if line[i] == '\\' and line[i+1] in ('\\', '\"', 'x'):
            if line[i] in ('\\', '\"'):
                add += 1

        print(line, add)
        i += 1
        # if i == 5: break

        diff2 += add + 2

print(diff)
print(diff2)
