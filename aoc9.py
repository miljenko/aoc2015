import re
from collections import defaultdict

def travel(current, visited=None, distance=None):
    if visited is None: visited = [current]
    if distance is None: distance = 0
    for neighbour in distances[current]:
        if neighbour in visited:
            continue
        # print current, neighbour
        cur_visited = visited + [neighbour]
        current_distance = distance + distances[current][neighbour]

        if len(cur_visited) == len(towns):
            # if (len(cur_visited) == 3):
            # print '!!!', cur_visited
            paths.append((current_distance, cur_visited))
        else:
            # print neighbour, visited, current_distance
            travel(neighbour, cur_visited, current_distance)


instr_re = re.compile('(\w+) to (\w+) = (\d+)')
distances = defaultdict(dict)

with open('input9.txt') as f:
    for line in f:
        match = instr_re.search(line)

        a, b, dist = match.groups()
        distances[a][b] = int(dist)
        distances[b][a] = int(dist)

towns = distances.keys()
paths = []

# travel(towns[0])
for town in towns:
    travel(town)

# for path in paths:
#  print path

print(distances)
print(len(paths))
print(min(dist for dist, path in paths))
print(max(dist for dist, path in paths))
