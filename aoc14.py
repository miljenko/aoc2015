import re

seconds = 2503
max_dist = 0
deers = {}

with open('input14.txt') as f:
    for line in f:
        deer = line.split()[0]
        speed, fly, rest = (int(x) for x in re.findall('\d+', line))
        deers[deer] = {'speed': speed, 'fly': fly,
                       'rest': rest, 'points': 0, 'distances': []}
        # print deer, speed, fly, rest
        repeats = seconds // (fly + rest)
        dist = (speed*fly)*repeats + min(seconds - repeats*(fly + rest), fly)*speed
        if dist > max_dist:
            max_dist = dist

        cur = 0
        for r in range(repeats):
            for i in range(fly):
                cur += speed
                deers[deer]['distances'].append(cur)
            for i in range(rest):
                deers[deer]['distances'].append(cur)
        for i in range(seconds - repeats*(fly + rest)):
            if i < fly:
                cur += speed
                deers[deer]['distances'].append(cur)
            else:
                deers[deer]['distances'].append(cur)

print(max_dist)

for i in range(seconds):
    sec_max = max(val['distances'][i] for deer, val in deers.items())
    for deer, val in deers.items():
        if val['distances'][i] == sec_max:
            val['points'] += 1

print(max(val['points'] for deer, val in deers.items()))
