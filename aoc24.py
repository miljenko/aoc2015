from functools import reduce
from itertools import combinations
from operator import mul

with open('input24.txt') as f:
    weights = {int(w) for line in f for w in line.split()}
print(weights)

groups = 4
required = sum(weights) // groups
minqe = float('inf')
found_min_g1_len = False

for n in range(1, len(weights) - groups - 1):
    print('len(g1) =', n)
    for g1 in combinations(weights, n):
        s1 = sum(g1)
        if s1 != required:
            continue
        qe = reduce(mul, g1)
        rest = weights - set(g1)
        groups_balanced = False
        for j in range(1, len(rest) - groups - 2):
            for g2 in combinations(rest, j):
                s2 = sum(g2)
                if s1 == s2:
                    # g3 = rest - set(g2)
                    # s3 = sum(g3)
                    # if s2 == s3:
                    found_min_g1_len = True
                    groups_balanced = True
                    if qe < minqe:
                        minqe = qe
                        print(minqe)
                    break
            if groups_balanced:
                break
    if found_min_g1_len:
        break

print(minqe)
