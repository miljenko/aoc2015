import re

instr_re = re.compile(
    '(?:(\w+) )??(?:(NOT|AND|OR|LSHIFT|RSHIFT) )??(\w+) -> (\w+)')

vars = {}
calculated = {}

def calc(v1, op, v2):
    if op is None:
        return v2
    elif op == 'NOT':
        return ~v2
    elif op == 'AND':
        return v1 & v2
    elif op == 'OR':
        return v1 | v2
    elif op == 'RSHIFT':
        return v1 >> v2
    elif op == 'LSHIFT':
        return v1 << v2

with open('input7_override.txt') as f:
    for line in f:
        match = instr_re.match(line)
        v1, op, v2, res = match.groups()
        try:
            v1 = int(v1)
        except:
            pass
        try:
            v2 = int(v2)
        except:
            pass

        if v1 is None and op is None and isinstance(v2, int):
            calculated[res] = v2

        vars[res] = [v1, op, v2]

    while len(calculated) != len(vars):
        for k, v in vars.items():
            if k not in calculated:
                v1, op, v2 = v

                if v1 in calculated:
                    v1 = calculated[v1]
                if v2 in calculated:
                    v2 = calculated[v2]
                vars[k] = [v1, op, v2]

                if (v1 is None or isinstance(v1, int)) and isinstance(v2, int):
                    calc_res = calc(v1, op, v2)
                    calculated[k] = calc_res

print((len(calculated)))
print(calculated)
print(calculated['a'])
