import random
from collections import defaultdict

repls = defaultdict(list)
mols = {}
reverserepls = {}
ends = []

with open('input19.txt') as f:
    for line in f:
        if not line:
            continue
        if '=>' not in line:
            input = line.strip()
            continue
        left, right = (r.strip() for r in line.split('=>'))
        repls[left].append(right)
        if left != 'e':
            reverserepls[right] = left
        else:
            ends.append(right)

# print repls
# print input

for k in repls:
    for r in repls[k]:
        for i in range(len(input)):
            if k in input[i:]:
                mols[input[:i] + input[i:].replace(k, r, 1)] = True

print(len(mols))

rs = list(reverserepls.keys())
r = 0
minr = 1000000

working = input
while True:
    k = random.choice(rs)
    found = False
    while k in working:
        found = True
        working = working.replace(k, reverserepls[k], 1)
        r += 1
        # print r, working
    if not found:
        rs.remove(k)
        if not rs:
            # print 'Dead end', working, r
            rs = list(reverserepls.keys())
            working = input
            r = 0
    else:
        rs = list(reverserepls.keys())
    if working in ends:
        # print 'Solution', working, r
        if r < minr:
            minr = r
            print('Minimum', working, minr + 1)
        working = input
        r = 0
