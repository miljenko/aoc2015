row = 2978
column = 3083
code = 20151125
limit = 2
found = False

while not found:
    rng = range(1, limit + 1)
    for c, r in zip(rng, reversed(rng)):
        code = (code * 252533) % 33554393
        if c == column and r == row:
            print(code)
            found = True
            break
    limit += 1
