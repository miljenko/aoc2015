from itertools import groupby

input = '1113222113'

def calc_groupby(input):
    for _ in range(50):
        input = ''.join(str(len(list(g))) + k for k, g in groupby(input))
    return len(input)

def calc(input):
    for _ in range(50):
        new_input = ''
        cur_char = None
        cur_len = 0
        for c in input:
            if c != cur_char:
                if cur_char is not None:
                    new_input += str(cur_len) + cur_char
                cur_char = c
                cur_len = 1
            else:
                cur_len += 1

        new_input += str(cur_len) + cur_char

        input = new_input
        # print _, len(input)

    return len(input)

print(calc_groupby(input))
print(calc(input))
