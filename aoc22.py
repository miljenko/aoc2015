import random

# solution part 1 = 1824, part 2 = 1937

boss = (71, 10, 0)  # hit, damage, armor
player = (50, 0, 0, 500)  # hit, damage, armor, mana

spells = [
    (53, 1, 4, 0, 0, 0),
    (73, 1, 2, 2, 0, 0),
    (113, 6, 0, 0, 7, 0),
    (173, 6, 3, 0, 0, 0),
    (229, 5, 0, 0, 0, 101)
]  # mana, time, damage, heal, armor, new mana

min_cost = min(spell[0] for spell in spells)

def attack(player, boss, part2=False):
    timers = [0, 0, 0]  # damage, armor, new_mana
    effects = [0, 0, 0]
    mana_spent = 0

    while True:  # do turns
        ### player turn
        if part2:
            player[0] -= 1
            if player[0] <= 0:
                return False, mana_spent

        # active spells
        boss[0] -= effects[0]  # damage
        player[3] += effects[2]  # new mana
        if boss[0] <= 0:
            return True, mana_spent

        # elapse timers
        for i in range(3):
            if timers[i] > 0:
                timers[i] -= 1
                if timers[i] == 0:
                    effects[i] = 0

        if player[3] < min_cost:  # can't afford any new spell - player loses
            return False, mana_spent

        # find a spell that can be cast
        spells_to_try = list(spells)
        while True:
            spell = random.choice(spells_to_try)
            can_cast = True
            if spell[0] > player[3]:  # can't afford this spell
                can_cast = False
            if spell[1] == 1:  # can always cast
                break
            if spell[2] > 0 and timers[0] > 0:  # damage
                can_cast = False
            if spell[4] > 0 and timers[1] > 0:  # armor
                can_cast = False
            if spell[5] > 0 and timers[2] > 0:  # new_mana
                can_cast = False
            if can_cast:
                break
            else:
                spells_to_try.remove(spell)
                if len(spells_to_try) == 0:
                    spell = None
                    break

        # add spell effects
        if spell is not None:
            mana_spent += spell[0]
            player[3] -= spell[0]  # reduce player mana
            if spell[1] == 1:  # instant
                boss[0] -= spell[2]  # damage
                if boss[0] <= 0:
                    return True, mana_spent
                player[0] += spell[3]  # heal
            else:
                if spell[2] > 0:  # damage
                    timers[0] = spell[1]
                    effects[0] = spell[2]
                elif spell[4] > 0:  # armor
                    timers[1] = spell[1]
                    effects[1] = spell[4]
                elif spell[5] > 0:  # new mana
                    timers[2] = spell[1]
                    effects[2] = spell[5]

        ### boss turn
        # active spells
        boss[0] -= effects[0]  # damage
        if boss[0] <= 0:
            return True, mana_spent
        player[3] += effects[2]  # new mana

        # elapse timers
        for i in range(3):
            if timers[i] > 0:
                timers[i] -= 1
                if timers[i] == 0:
                    effects[i] = 0

        # boss attack
        player[0] -= max(boss[1] - effects[1], 1)  # boss damage - player armor
        if player[0] <= 0:
            return False, mana_spent

minpw = 1000000
sims = 0

while sims <= 10000000:
    sims += 1
    p = list(player)
    b = list(boss)
    won, cost = attack(p, b, part2=True)
    if won and (cost < minpw):
        minpw = cost
        print('New minimum', minpw)
    if sims % 100000 == 0:
        print('Done', sims, 'simulations')

print('Minimum cost to win =', minpw)
