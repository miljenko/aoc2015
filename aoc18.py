size = 100
steps = 100

def calc(grid, i, j):
    n = 0
    for ni in range(i - 1, i + 2):
        if ni < 0 or ni >= size:
            continue
        for nj in range(j - 1, j + 2):
            if nj < 0 or nj >= size or (ni == i and nj == j):
                continue
            n += grid[ni][nj]

    if (i == 0 or i == size - 1) and (j == 0 or j == size - 1):
        return 1
    elif grid[i][j] == 1:
        val = 1 if n == 2 or n == 3 else 0
    else:
        val = 1 if n == 3 else 0

    return val

grid = [[0 for _ in range(size)] for _ in range(size)]

with open('input18.txt') as f:
    for i, line in enumerate(f):
        for j, c in enumerate(line.strip()):
            if (i == 0 or i == size - 1) and (j == 0 or j == size - 1):
                grid[i][j] = 1
            else:
                grid[i][j] = 1 if c == '#' else 0

for i in range(steps):
    next_grid = [[0 for _ in range(size)] for _ in range(size)]

    for i in range(size):
        for j in range(size):
            next_grid[i][j] = calc(grid, i, j)

    grid = next_grid

print(sum(sum(row) for row in grid))
