import re

grid = [[0 for _ in range(1000)] for _ in range(1000)]

instr_re = re.compile('(\d+),(\d+) through (\d+),(\d+)')

with open('input6.txt') as f:
    for line in f:
        if line.startswith('turn off'):
            #op = lambda x: 0
            op = lambda x: max(x - 1, 0)
        elif line.startswith('turn on'):
            #op = lambda x: 1
            op = lambda x: x + 1
        elif line.startswith('toggle'):
            #op = lambda x: x ^ 1
            op = lambda x: x + 2

        match = instr_re.search(line)

        startx, starty, endx, endy = (int(x) for x in match.groups())

        for x in range(startx, endx + 1):
            for y in range(starty, endy + 1):
                grid[x][y] = op(grid[x][y])

print(sum(sum(row) for row in grid))
