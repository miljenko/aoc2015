from functools import reduce
from operator import mul

total = 0
ribbon = 0

with open('input2.txt') as f:
    for line in f:
        lens = [int(l) for l in line.split('x')]
        lens.sort()
        surfaces = [lens[0]*lens[1], lens[1]*lens[2], lens[2]*lens[0]]
        total += 2*sum(surfaces) + min(surfaces)
        ribbon += 2*(lens[0] + lens[1]) + reduce(mul, lens)

print(total)
print(ribbon)
