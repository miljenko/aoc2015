from collections import defaultdict
from itertools import permutations

happiness = defaultdict(dict)

with open('input13.txt') as f:
    for line in f:
        at1, _, op, n, _, _, _, _, _, _, at2 = line.split()
        n = int(n)
        at2 = at2[:-1]
        if op == 'lose':
            n = -n
        print(at1, op, n, at2)
        happiness[at1][at2] = n

for k in list(happiness.keys()):
    happiness['me'][k] = 0
    happiness[k]['me'] = 0

print(happiness)
max_happiness = 0

for p in permutations(happiness.keys()):
    h = sum(happiness[p[i]][p[i + 1]] + happiness[p[i + 1]][p[i]]
            for i in range(len(p) - 1))
    h += happiness[p[0]][p[-1]] + happiness[p[-1]][p[0]]
    if h > max_happiness:
        max_happiness = h

print(max_happiness)
