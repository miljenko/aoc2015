from collections import defaultdict
from itertools import chain, combinations

def powerset(iterable):
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))

total = 150
combos = 0
combos_by_len = defaultdict(int)

with open('input17.txt') as f:
    containers = [int(line) for line in f]

for s in powerset(containers):
    if sum(s) == total:
        combos += 1
        combos_by_len[len(s)] += 1

print(combos)
print(combos_by_len)
print(combos_by_len[min(k for k in combos_by_len)])
