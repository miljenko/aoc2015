import json

def sumitems(item):
    total = 0
    if isinstance(item, list):
        for subitem in item:
            total += sumitems(subitem)
    elif isinstance(item, dict):
        for k, v in item.items():
            if v == 'red':
                return 0
        for k, v in item.items():
            total += sumitems(v)
    elif isinstance(item, int):
        total += item

    return total

with open('input12.txt') as f:
    nums = json.load(f)

n = sumitems(nums)
print(n)
