posx1 = 0
posy1 = 0
posx2 = 0
posy2 = 0
i = 0
visited = {(0, 0): True}

def calc_pos(posx, posy, move):
    if move == '^':
        posy += 1
    elif move == 'v':
        posy -= 1
    elif move == '>':
        posx += 1
    elif move == '<':
        posx -= 1

    return (posx, posy)

with open('input3.txt') as f:
    for line in f:
        for c in line:
            if i % 2 == 0:
                posx1, posy1 = calc_pos(posx1, posy1, c)
                visited[(posx1, posy1)] = True
            else:
                posx2, posy2 = calc_pos(posx2, posy2, c)
                visited[(posx2, posy2)] = True

            i += 1

print(len(visited))
