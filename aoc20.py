input = 36000000

l = [10 for _ in range(1000000)]

for step in range(2, 1000000):
    n = 0
    for i in range(step, 1000000, step):
        l[i] += step * 11
        n += 1
        if n == 50:
            break

for i, presents in enumerate(l):
    if presents >= input:
        print(i, presents)
        break
